# Runbooks

This repo will be used for githost runbooks. Right now they are very sparse, but eventually
they will be robust and linked to from AlertManager during a page.


# Alerts

To add alerts, please see the [infrastructure runbook repo](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/alerts_manual.md).
This is the same process except substitute GitHost repos. You can mimic
the [alerts we already have](https://gitlab.com/gitlab-com/githost-runbooks/tree/master/alerts) 
as well.

# Chef repo

https://dev.gitlab.org/githost-cookbooks/chef-repo

# Chef server

https://swedish.chef.gitlab.com/organizations/githost

# Contributing 

[See CONTRIBUTING.md](https://gitlab.com/gitlab-com/githost-runbooks/blob/update-CLA/CONTRIBUTING.md)