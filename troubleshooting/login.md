# Login
To login into a githost instance you will need ssh and access to githost.io.

## SSH
Login into githost.io with your normal posix user account.

If you can't ask in `#infrastructure`

Become root and use `/root/.ssh/githost_id_rsa` key to login into an instance, i.e.:
```
jeroen@xps15:~$ ssh githost.io
* snip *
jeroen@prod-www01:~$ sudo su -
root@prod-www01:~# ssh -i /root/.ssh/githost_id_rsa litgroup.githost.io
* snip *
root@litgroup:~# 
```

## SSH login fails for an instance
Login into Digital Ocean and reset root password for that instance.

After reset add /root/.ssh/githost_id_rsa.pub to /root/.ssh/authorized_keys and make sure password authentication is disabled for ssh on that instance.
